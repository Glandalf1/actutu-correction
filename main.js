const livres = [
    /*{
        titre: 'Livre 1',
        auteur: 'John DOE',
        prix: 34,
        image: null,
        resume: 'lorem  ipsum erljghekrg zerkj zkjrgheirgh z.',
    },
    {
        titre: 'Harry POTTER',
        auteur: 'JK ROLIERNGEJRB',
        prix: 45,
        image: null,
        resume: 'Vodldemort, blabla',
    },
    {
        titre: 'Livre 3',
        auteur: 'John DOE',
        prix: 34,
        image: null,
        resume: 'lorem  ipsum erljghekrg zerkj zkjrgheirgh z.',
    },
    {
        titre: 'Harry POTTER II',
        auteur: 'JK ROLIERNGEJRB',
        prix: 45,
        image: null,
        resume: 'Vodldemort, blabla',
    }*/
];

start()

function start() {
    initSubmitForm()
    RechercheLivres('javascript')
}

function afficheRecommandations() {
    /**
     * On veut itérer sur notre tableau de livres, on peut le faire au moins avec trois types de `for`
     * On laisse ci-dessous les deux que l'on estime les moins pertinentes puis on utilise
     * finalement le classique for le plus simple car il permet de nous arrêter quand on le souhaite
     * de manière plus claire.
     * 
     ** Méthode 1:  
     * let compteur = 0;
     * for (let livre of livres) {
     *     console.log(livre)
     *     if (compteur++ > 3) { 
     *         break; 
     *     }
     * }
     *
     ** Méthode 2
     * livres.forEach(livre => {
     *     console.log(livre)
     * })
     */
   
    // On vide la partie HTML des recommendations pour commencer
    document.querySelector('#recommendations').innerHTML = ''
    // Pas parfait, mais mélange notre tableau de livres
    livres.sort(function (a, b) { return 0.5 - Math.random() })
    // Affiche les trois premiers livres de notre tableau mélangé (méthode 3)
    for (let i=0; (i < livres.length & i < 3) ; i++) {
        let livre = livres[i]
        const livreHTML = document.createElement('li')
        const titreHTML = document.createElement('h3')
        const auteurHTML = document.createElement('span')
        const prixHTML = document.createElement('span')
        const descriptionHTML = document.createElement('p')
        const imgHTML = document.createElement('img')

        livreHTML.classList.add('livre')
        auteurHTML.classList.add('auteur')
        prixHTML.classList.add('prix')
        descriptionHTML.classList.add('description')

        titreHTML.innerText = livre.titre
        auteurHTML.innerText = livre.auteur
        prixHTML.innerText = livre.prix + ' €'
        descriptionHTML.innerText = livre.resume
        imgHTML.src = livre.image

        livreHTML.append(titreHTML)
        livreHTML.append(auteurHTML)
        livreHTML.append(prixHTML)
        livreHTML.append(descriptionHTML)
        // On insère l'image seulement si elle n'est pas nulle
        livre.image? livreHTML.append(imgHTML): null;

        document.querySelector('#recommendations').append(livreHTML)
    }
}

function afficheLivres(prixMax) {

    document.querySelector('#resultats').innerHTML = ''
    // Parcours de tous les livres 
    for (let i=0; i < livres.length ; i++) {
        let livre = livres[i]
        // Si le livre n'a pas de prix connu, ou que son prix est supérieur au prix max, on le zappe
        if(livre.prix > prixMax || !livre.prix) {
            continue;
        }
        const livreHTML = document.createElement('li')
        const titreHTML = document.createElement('h3')
        const auteurHTML = document.createElement('span')
        const prixHTML = document.createElement('span')
        const descriptionHTML = document.createElement('p')
        const imgHTML = document.createElement('img')

        livreHTML.classList.add('livre')
        auteurHTML.classList.add('auteur')
        prixHTML.classList.add('prix')
        descriptionHTML.classList.add('description')

        titreHTML.innerText = livre.titre
        auteurHTML.innerText = livre.auteur
        prixHTML.innerText = livre.prix + ' €'
        descriptionHTML.innerText = livre.resume
        imgHTML.src = livre.image

        livreHTML.append(titreHTML)
        livreHTML.append(auteurHTML)
        livreHTML.append(prixHTML)
        livreHTML.append(descriptionHTML)
        // On insère l'image seulement si elle n'est pas nulle
        livre.image? livreHTML.append(imgHTML): null;

        document.querySelector('#resultats').append(livreHTML)
    }
}

function RechercheLivres(recherche) {
    // Envoyer la requête à Google API
    fetch(`https://www.googleapis.com/books/v1/volumes?q=${recherche}`)
        .then(response => response.json())
        .then(data => {
            const livresAPI = data.items
            livres.splice(0, livres.length)
            // Mettre les résultats au format JSON de nos livres (déclarés en haut de fichier)
            livresAPI.forEach(livre => {
                const livreFormatte = {
                    titre: livre.volumeInfo?.title,
                    auteur: livre.volumeInfo?.authors?.join(', '),
                    // prix: livre.saleInfo?.retailPrice?.amount,
                    prix: livre.saleInfo?.listPrice?.amount,
                    image: livre.volumeInfo?.imageLinks?.thumbnail,
                    resume: livre.volumeInfo?.description,
                }
                // Les stocker dans la variable `livres` du début
                livres.push(livreFormatte)
            });
            // Mettre à jour les infos affichées pour l'utilisateur
            afficheRecommandations()
            afficheLivres(20)
        })
}

function initSubmitForm() {
    document.querySelector('#form1').addEventListener('submit', (ev) => {
        ev.preventDefault()
        RechercheLivres(document.querySelector('#champ-recherche').value)
    })
}
